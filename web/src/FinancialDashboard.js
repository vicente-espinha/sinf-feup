import React, { Component } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import { Bar } from 'react-chartjs-2';
import './MainDashboard.css';
import axios from 'axios';
import Select from 'react-select';

class FinancialDashboard extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            isValidDate: true,
            startDate: new Date('January 1 2018 00:00:00'),
            endDate: new Date('December 1 2018 00:00:00'),
            month: 11,
            hasBS: false,
            optionsMonth: 
            [
                { value: '0', label: 'January' },
                { value: '1', label: 'February' },
                { value: '2', label: 'March' },
                { value: '3', label: 'April' },
                { value: '4', label: 'May' },
                { value: '5', label: 'June' },
                { value: '6', label: 'July' },
                { value: '7', label: 'August' },
                { value: '8', label: 'Setember' },
                { value: '9', label: 'October' },
                { value: '10', label: 'November' },
                { value: '11', label: 'December' }
            ],
            cash: 0,
            bank: 0,
            AP: 0,
            AR: 0,
            revenue: 0,
            cogs: 0,
            grossProfitMargin: 0,
            salesVolume: [0,0,0,0,0,0,0,0,0,0,0,0]
        }
        
        this.getInfoSaft = this.getInfoSaft.bind(this);
        this.getCashFromApi = this.getCashFromApi.bind(this);
        this.getBankFromApi = this.getBankFromApi.bind(this);
        this.getAPFromApi = this.getAPFromApi.bind(this);
        this.getARFromApi = this.getARFromApi.bind(this);
        this.getCOGSFromApi = this.getCOGSFromApi.bind(this);
        this.getSalesVolumeFromApi = this.getSalesVolumeFromApi.bind(this);
        this.getRevenueFromApi = this.getRevenueFromApi.bind(this);
        this.getGrossProfitMargin = this.getGrossProfitMargin.bind(this);
    }
    
    componentDidMount(){
        if(this.props.hasSaft) {
            this.getInfoSaft();
        }
    }
    
    getInfoSaft() {
        if(this.state.month === null)
            return;

        console.log("Adquiring info from saft...");

        this.getBalanceteFromApi();
        this.getCashFromApi();
        this.getBankFromApi();
        this.getAPFromApi();
        this.getARFromApi();
        this.getCOGSFromApi();
        this.getSalesVolumeFromApi();
        this.getRevenueFromApi();
        this.getEBITDAFromApi();
    }

    getEBITDAFromApi(){
        let self = this;

        axios.get('http://localhost:3030/getEBITDA?month=' + this.state.month)
        .then(function(response) {
            self.setState({ ebitda: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    getBalanceteFromApi(){
        let self = this;

        axios.get('http://localhost:3030/getBalancete')
        .then(function(response) {
            self.setState({ hasBS: true });
        })
        .catch(function(error) {
            console.log(error);
            self.setState({ hasBS: false });
        });
    }

    getCashFromApi(){
        let self = this;

        axios.get('http://localhost:3030/getCash?month=' + this.state.month)
        .then(function(response) {
            self.setState({ cash: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    getRevenue(){
        return isNaN(this.state.revenue[this.state.month]) ? 0 : this.state.revenue[this.state.month];
    }

    getRevenueFromApi(){
        let self = this;

        axios.get('http://localhost:3030/getRevenue?month=' + this.state.month)
        .then(function(response) {
            self.setState({ revenue:response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    getGrossProfitMargin(){
        let gpm = (this.state.revenue[this.state.month] - this.state.cogs)/this.state.revenue[this.state.month];

        return isNaN(gpm) ? 0 : ((gpm*100).toFixed(2) + "%");
    }
    
    getBankFromApi(){
        let self = this;
        axios.get('http://localhost:3030/getBank?month=' + this.state.month)
        .then(function(response) {
            self.setState({ bank: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    getAPFromApi(){
        let self = this;
        
        axios.get('http://localhost:3030/getAP?month=' + this.state.month)
        .then(function(response) {
            self.setState({ AP: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    getARFromApi(){
        let self = this;
        axios.get('http://localhost:3030/getAR?month=' + this.state.month)
        .then(function(response) {
            self.setState({ AR: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    getCOGSFromApi(){
        let self = this;
        axios.get('http://localhost:3030/getCOGS?month=' + this.state.month)
        .then(function(response) {
            self.setState({ cogs: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    async getSalesVolumeFromApi(){
        let self = this;
        await axios.get('http://localhost:3030/getSalesVolume')
        .then(function(response) {
            self.setState({ salesVolume: response.data });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
 
    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }
    
    componentDidUpdate(prevProps) {
        if (!prevProps.hasSaft && this.props.hasSaft) {
            this.getInfoSaft();
        }
    }
    
    onChangeMonth = async month => {
        this.value = month.value; 
        await this.setState({ month:month.value });
        this.getInfoSaft();
    };
        
    render() {
        
        let data_bar = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                label: 'Sales Volume',
                data: this.state.salesVolume,
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }],
            options: {
                title: {
                    display: true,
                    position: 'top',
                    text: 'Sales Volume'
                }
            }
        }
        
        return (
            <div>
                <div className="align-right">
                    <Select className="dateInput" onChange={this.onChangeMonth} placeholder='Month' search 
                        selection options={this.state.optionsMonth} defaultValue={this.state.optionsMonth[this.state.month]}/>
                </div>
            <Grid columns={2}>
                <Grid.Row stretched>
                    <Grid.Column width={5}>
                        <Segment style={{ height: 150 }}>
                            <h4>Sales Revenue</h4>
                            <p className="kpi">{this.getPrettyNumber(this.getRevenue())}</p>
                        </Segment>
                        <Segment style={{ height: 150 }}>
                            <h4>Accounts Payable</h4>
                            <p className="kpi">{this.getPrettyNumber(this.state.AP)}</p>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width={5}>
                        <Segment style={{ height: 150 }}>
                            <h4>Cost of Goods Sold</h4>
                            <p className="kpi">{this.getPrettyNumber(this.state.cogs)}</p>
                        </Segment>
                        <Segment style={{ height: 150 }}>
                            <h4>Accounts Receivable</h4>
                            <p className="kpi">{this.getPrettyNumber(this.state.AR)}</p>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width={5}>
                        <Segment style={{ height: 150 }}>
                            <h4>Gross Profit Margin</h4>
                            <p className="kpi">{this.getGrossProfitMargin()}</p>
                        </Segment>
                        <Segment style={{ height: 150 }}>
                            <h4>EBITDA</h4>
                            <p className="kpi">{this.getPrettyNumber(this.state.ebitda)}</p>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row stretched>
                    <Grid.Column width={10}>
                        <Segment style={{ height: 150 }}>
                            <Bar
                            data={data_bar}
                            options={{
                                maintainAspectRatio: false
                            }}
                            />
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width={5}>
                        <Segment style={{ height: 150 }}>
                            <h4>Cash / Bank</h4>
                            <p className="kpi">{this.getPrettyNumber(this.state.cash)} / {this.getPrettyNumber(this.state.bank)}</p>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            </div>
            );
        }
    }
    
    export default FinancialDashboard;
    