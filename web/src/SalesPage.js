import React, { Component } from 'react';
import { Grid, Segment, GridColumn, GridRow } from 'semantic-ui-react';
import ReactTable from "react-table";
import { Link } from 'react-router-dom';
import axios from 'axios';
import "react-table/react-table.css";
import "./SalesPage.css";

class SalesPage extends Component {

    constructor(props) {
        super(props);

        console.log(props);
        this.state = {
            id: props.match.params.id,
            info: props.location.state.saleInfo,
        }

        this.handleLineChange = this.handleLineChange.bind(this);
        this.getProdInfo = this.getProdInfo.bind(this);

        console.log(props.location.state.saleInfo);
    }

    componentDidMount() {
        let customInfo = this.state.info;
        if (customInfo.DocumentStatus.InvoiceStatus == "N")
            customInfo.DocumentStatus.InvoiceStatus = "Normal";
        else if (customInfo.DocumentStatus.InvoiceStatus == "A")
            customInfo.DocumentStatus.InvoiceStatus = "Annuled Document";
        else if (customInfo.DocumentStatus.InvoiceStatus == "F")
            customInfo.DocumentStatus.InvoiceStatus = "Self Invoiced";
        else if (customInfo.DocumentStatus.InvoiceStatus == "N")
            customInfo.DocumentStatus.InvoiceStatus = "Invoiced Document";

        this.setState({ info: customInfo })
    }

    handleLineChange(event) {
        this.setState({ selectedLine: event });
    }

    getProdInfo(prodName) {
        for (let i = 0; i < this.props.products.length; i++) {
            if (this.props.products[i].Artigo === prodName) {
                return { PVP1: parseFloat(this.props.products[i].PVP1).toFixed(2), StkActual: this.props.products[i].StkActual }
            }
        }
    }


    render() {

        if (this.state.info === null) {
            return (
                <div className="loading">
                    <div className="loader"></div>
                </div>
            );
        }

        let infoLines = [];
        if(this.state.info !== undefined){
            infoLines = this.state.info.Line.map(line => {
                return { ...line, UnitPrice: line.UnitPrice.toFixed(2) }
            });
        }

        const columns = [
            {
                Header: 'Product Code',
                accessor: 'ProductCode',
                Cell: ({ row }) =>
                    (<Link to={{
                        pathname: `/products/${row.ProductCode}`,
                        state: { prodInfo: this.getProdInfo(row.ProductCode), sales: this.props.location.state.sales }
                    }}>
                        {row.ProductCode}
                    </Link>)
            },
            {
                Header: 'Credit Amount',
                accessor: 'CreditAmount' // String-based value accessors!
            },
            {
                Header: 'Description',
                accessor: 'Description'
            },
            {
                Header: 'Quantity',
                accessor: 'Quantity'
            },
            {
                Header: 'Unit Price',
                accessor: 'UnitPrice'
            }]

        return (
            <div>
                <Grid columns={2}>
                    <Grid.Row stretched>
                        <Grid.Column width={6}>
                            <Segment>
                                <h1>Sale: {this.state.id}</h1>
                                <p>Source ID: {this.state.info.DocumentStatus.SourceID}</p>
                                <p>Customer ID: <Link to={{
                                    pathname: `/customers/${this.state.info.clientName}`,
                                    state: { sales: this.props.location.state.sales }
                                }}>{this.state.info.CustomerID}</Link>
                                </p>
                                <p>Client Name: {this.state.info.clientName}</p>
                                <h2>Document Totals</h2>
                                <p>Gross Total: {parseFloat(this.state.info.DocumentTotals.GrossTotal).toFixed(2)}</p>
                                <p>NetTotal Total: {parseFloat(this.state.info.DocumentTotals.NetTotal).toFixed(2)}</p>
                                <p>Tax Payable: {parseFloat(this.state.info.DocumentTotals.TaxPayable).toFixed(2)}</p>
                                <h2>Invoice Status</h2>
                                <p>Invoice Status Date: {this.state.info.InvoiceDate}</p>
                                <p>Invoice Status: {this.state.info.DocumentStatus.InvoiceStatus}</p>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Segment height={10}>
                                <h2>Line Information</h2>
                                <ReactTable
                                    data={infoLines}
                                    columns={columns}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                        </Grid.Column>


                    </Grid.Row>

                </Grid>
                <div className="ship">
                    <Segment >
                        <h1>Shipping Information</h1>
                        <Grid columns={2}>
                            <GridRow >
                                <GridColumn>
                                    <h2>Ship From:</h2>
                                    <p>Address: {this.state.info.ShipFrom.Address.AddressDetail}</p>
                                    <p>Country: {this.state.info.ShipFrom.Address.Country}</p>
                                    <p>Address: {this.state.info.ShipFrom.Address.City}</p>

                                </GridColumn>
                                <GridColumn>
                                    <h2>Ship To:</h2>
                                    <p>Address: {this.state.info.ShipTo.Address.AddressDetail}</p>
                                    <p>Country: {this.state.info.ShipTo.Address.Country}</p>
                                    <p>Address: {this.state.info.ShipTo.Address.City}</p>
                                </GridColumn>
                            </GridRow>
                        </Grid>
                        <p>Delivery Date: {this.state.info.ShipFrom.DeliveryDate}</p>


                    </Segment>
                </div>
            </div>
        );

    }

}


export default SalesPage;