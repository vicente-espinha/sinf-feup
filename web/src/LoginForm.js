import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Grid } from 'semantic-ui-react';
import './LogInForm.css';

class LoginForm extends Component {


  constructor(props) {
    super(props);

    this.state = {
      redirect: false
    }
  }

  // Using a class based component here because we're accessing DOM refs
  handleSignIn(e) {
    e.preventDefault();
    let username = this.refs.username.value;
    let password = this.refs.password.value;
    let company = this.refs.company.value;
    this.props.getToken(username, password, company)
    this.setState({ redirect: true });

  }


  render() {
    if (this.state.redirect) {
      return <Redirect from="/" to="/main" push />;
    }
    return (

      <Grid>
        <Grid.Column width={5}></Grid.Column>
        <Grid.Column stretched width={6} className="login-form-container">
          <Form onSubmit={this.handleSignIn.bind(this)} className="login-form">
            <h3>Sign in</h3>
            <Form.Field>
              <label>Username</label>
              <input type="text" ref="username" placeholder="Enter you username" />
            </Form.Field>
            <Form.Field>
              <label>Company</label>
              <input type="text" ref="company" placeholder="Enter you company" />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input type="password" ref="password" placeholder="Enter password" />
            </Form.Field>
            <Button type='submit'>Submit</Button>
          </Form>
        </Grid.Column>
        <Grid.Column width={5}></Grid.Column>
      </Grid>

    );
  }

}

export default LoginForm;