import React, { Component } from 'react';
import { Grid, Segment, Card } from 'semantic-ui-react';
import { Doughnut } from 'react-chartjs-2';
import ReactTable from "react-table";
import { Link } from 'react-router-dom';
import axios from 'axios';
import './ClientPage.css';

class ClientPage extends Component {

    constructor(props) {
        super(props);

        console.log(props);

        this.state = {
            companyName: props.match.params.id,
            allSales: props.location.state.sales.slice(0),
            info: null,
            top5Products: [],
            totalValue: 0
        }

        this.getClientInfo = this.getClientInfo.bind(this);
        this.getTop5ProductsAndValue = this.getTop5ProductsAndValue.bind(this);
        this.getSaleInfo = this.getSaleInfo.bind(this);

    }

    async componentDidMount() {
        await this.getClientInfo();
        this.getTop5ProductsAndValue();
    }

    async getClientInfo() {

        await axios.get("http://localhost:3030/getClientInfo?companyName=" + this.state.companyName)
            .then((res) => {
                console.log(res);
                this.setState({ info: res.data });
            })
            .catch((err) => console.log(err))
    }

    getTop5ProductsAndValue() {

        let clientSales = this.state.info.invoices;
        let topProducts = [];
        let totalValue = 0;

        for (let i = 0; i < clientSales.length; i++) {

            totalValue += clientSales[i].DocumentTotals.NetTotal;

            for (let j = 0; j < clientSales[i].Line.length; j++) {

                topProducts.push({
                    name: clientSales[i].Line[j].ProductCode,
                    value: clientSales[i].Line[j].Quantity
                });

            }

        }

        let sortedProducts = topProducts.sort(function (a, b) {
            return (a.value > b.value) ? -1 : ((b.value > a.value) ? 1 : 0)
        });

        this.setState({ top5Products: sortedProducts, totalValue });

    }

    getSaleInfo(invoice_no) {

        let sale;

        for (let i = 0; i < this.state.allSales.length; i++) {

            console.log(this.state.allSales[i].InvoiceNo + "  - " + invoice_no);
            if (this.state.allSales[i].InvoiceNo === invoice_no) {
                sale = this.state.allSales[i];
            }

        }

        return sale;

    }

    getPrettyNumber(number){
        if (number < 1000) 
        return "€" + number.toFixed(2)+ "";
        else if (number > 1000 && number < 1000000) 
        return "€" + (number/1000).toFixed(2) + "K";
        else if (number > 1000000) 
        return "€" + (number/1000000).toFixed(2) + "M";
        
        return "";
    }


    render() {

        if (this.state.info === null) {
            return (
                <div className="loading">
                    <div className="loader"></div>
                </div>
            );
        }

        let invoices = this.state.info.invoices.map(sale => ({
            id: sale.InvoiceNo, type: sale.InvoiceType,
            date: sale.InvoiceDate, netTotal: sale.DocumentTotals.NetTotal.toFixed(2), grossTotal: sale.DocumentTotals.GrossTotal.toFixed(2)
        }));

        let products_pie = {
            labels: this.state.top5Products.map(p => p.name).slice(0, 5),
            datasets: [{
                label: '# of Votes',
                data: this.state.top5Products.map(p => p.value).slice(0, 5),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(0, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(0, 102, 255, 0.2)'
                ],
                borderWidth: 1
            }]
        }

        console.log(this.state.info);

        return (
            <div>
                <Grid columns={2}>
                    <Grid.Row stretched>
                        <Grid.Column width={6}>
                            <Segment>
                                <h2>{this.state.companyName}</h2>
                                <p><strong>Customer ID: </strong> {this.state.info.CustomerID}</p>
                                <Card className="align_left">
                                    <Card.Content>
                                        <Card.Header>Billing Address</Card.Header>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <p><strong>Address Detail: </strong>{this.state.info.BillingAddress.AddressDetail}</p>
                                        <p><strong>City: </strong>{this.state.info.BillingAddress.City}</p>
                                        <p><strong>Postal Code: </strong>{this.state.info.BillingAddress.PostalCode}</p>
                                        <p><strong>Country: </strong>{this.state.info.BillingAddress.Country}</p>
                                    </Card.Content>
                                </Card>
                                <Card className="align_left">
                                    <Card.Content>
                                        <Card.Header>Ship to Address</Card.Header>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <p><strong>Address Detail: </strong>{this.state.info.ShipToAddress.AddressDetail}</p>
                                        <p><strong>City: </strong>{this.state.info.ShipToAddress.City}</p>
                                        <p><strong>Postal Code: </strong>{this.state.info.ShipToAddress.PostalCode}</p>
                                        <p><strong>Country: </strong>{this.state.info.ShipToAddress.Country}</p>
                                    </Card.Content>
                                </Card>
                                {this.state.info.Telephone !== undefined || this.state.info.Fax !== undefined || this.state.info.Website !== undefined ?
                                    <Card className="align_left">
                                        <Card.Content>
                                            <Card.Header>Contacts</Card.Header>
                                        </Card.Content>
                                        <Card.Content extra>
                                            {this.state.info.Telephone !== undefined ? <p><strong>Telephone: </strong>{this.state.info.Telephone}</p> : ""}
                                            {this.state.info.Fax !== undefined ? <p><strong>Fax: </strong>{this.state.info.Fax}</p> : ""}
                                            {this.state.info.Website !== undefined ? <p><strong>Website: </strong>{this.state.info.Website}</p> : ""}
                                        </Card.Content>
                                    </Card>
                                    : ""}
                            </Segment>

                            <Segment style={{ height: 150 }} >
                                <h4 style={{ height: 50 }}>Cash Spent by Customer</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.totalValue)}</p>
                            </Segment>

                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Segment height={10}>
                                <ReactTable
                                    data={invoices}
                                    columns={[
                                        {
                                            Header: "Sale ID",
                                            accessor: "id",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/sales/${row.id}`,
                                                    state: { saleInfo: this.getSaleInfo(row.id), sales: this.state.allSales }
                                                }}>
                                                    {row.id}
                                                </Link>)
                                        },
                                        {
                                            Header: "Type",
                                            accessor: "type"
                                        },
                                        {
                                            Header: "Date",
                                            accessor: "date",
                                        },
                                        {
                                            Header: "Net Total €",
                                            accessor: "netTotal"
                                        },
                                        {
                                            Header: "Gross Total €",
                                            accessor: "grossTotal"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                            <Segment height={5}>
                                <h4>Top 5 Products of Customer</h4>
                                <Doughnut
                                    data={products_pie}
                                    width={30}
                                    height={15}
                                />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );

    }

}


export default ClientPage;