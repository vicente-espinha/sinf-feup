import React, { Component } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import ReactTable from "react-table";
import { Link } from 'react-router-dom';
import axios from 'axios';

class ProductsPage extends Component {

    constructor(props) {
        super(props);

        console.log(props);

        this.state = {
            id: props.match.params.id,
            sales: props.location.state.sales,
            info: null,
            PVP1: props.location.state.prodInfo.PVP1,
            StkActual: props.location.state.prodInfo.StkActual,
            totalCreditAmount: 0,
            productSales: [],
            productSuppliers: [],
            productSuppliersFullInfo: []
        }

        this.getSupplierInfo = this.getSupplierInfo.bind(this);
        this.getProductInfo = this.getProductInfo.bind(this);
        this.getProductSales = this.getProductSales.bind(this);
        this.getSuppliers = this.getSuppliers.bind(this);

    }

    componentDidMount() {
        this.getProductInfo();
        this.getProductSales();
        this.getSuppliers();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.token !== this.props.token) {
            this.getProductInfo();
            this.getSuppliers();
        }
    }

    getProductInfo() {
        axios.get("http://localhost:2018/WebApi/Base/Artigos/Edita/" + this.state.id, {
            headers: {
                'Authorization': "bearer " + this.props.token
            }
        })
            .then((res) => {
                console.log("product info", res.data);
                this.setState({ info: res.data });
            })
            .catch((err) => {
                console.log(err);
            })
    }

    getProductSales() {

        axios.get("http://localhost:3030/getProductSales?id=" + this.state.id)
            .then((res) => this.setState({ productSales: res.data.sales, totalCreditAmount: res.data.creditAmount }))
            .catch((err) => console.log(err))

    }

    async getSuppliers() {

        await axios({
            method: 'POST',
            url: "http://localhost:2018/WebApi/Administrador/Consulta",
            data: "\"SELECT Artigo, LinhasCompras.DataDoc, LinhasCompras.DataEntrada, LinhasCompras.DataEntrega, Fornecedores.Fornecedor, LinhasCompras.Quantidade, " +
                "LinhasCompras.PrecUnit, LinhasCompras.PrecoLiquido, Fornecedores.Nome FROM LinhasCompras INNER JOIN CabecCompras " +
                "ON LinhasCompras.IdCabecCompras = CabecCompras.Id INNER JOIN Fornecedores ON CabecCompras.Entidade = Fornecedores.Fornecedor WHERE Artigo = '" + this.state.id + "'\"",
            headers: {
                'Authorization': "bearer " + this.props.token,
                "Content-Type": "application/json"
            },
        })
            .then((res) => {
                let supps = res.data.DataSet.Table.map(sup => {
                    return { ...sup, PrecUnit: sup.PrecUnit.toFixed(2), PrecoLiquido: sup.PrecoLiquido.toFixed(2) }
                })
                this.setState({ productSuppliers: supps });
            })
            .catch((err) => {
                console.log(err);
            })

    }

    getSaleInfo(invoice_no) {

        let sale;

        for (let i = 0; i < this.state.sales.length; i++) {

            if (this.state.sales[i].InvoiceNo === invoice_no) {
                sale = this.state.sales[i];
            }

        }


        return sale;

    }

    getSupplierInfo(supplier_name) {
        for (let i = 0; i < this.state.productSuppliers.length; i++) {
            if (this.state.productSuppliers[i].Fornecedor === supplier_name) {
                return this.state.productSuppliers[i];;
            }
        }
    }

    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }


    render() {

        if (this.state.info === null) {
            return (
                <div className="loading">
                    <div className="loader"></div>
                </div>
            );
        }

        let invoices = this.state.productSales.map(sale => {

            for (let i = 0; i < sale.Line.length; i++) {

                if (sale.Line[i].ProductCode === this.state.id) {
                    return {
                        id: sale.InvoiceNo, type: sale.InvoiceType,
                        date: sale.InvoiceDate, quantity: sale.Line[i].Quantity, unitPrice: sale.Line[i].UnitPrice.toFixed(2), creditAmount: sale.Line[i].CreditAmount.toFixed(2)
                    }
                }

            }
        });

        return (
            <div>
                <Grid columns={2}>
                    <Grid.Row stretched>
                        <Grid.Column width={6}>
                            <Segment>
                                <h1>Products: {this.state.id}</h1>
                                <p>Description: {this.state.info.Descricao}</p>
                                <p>Last Entry Date: {this.state.info.DataUltimaEntrada}</p>
                                <p>Last Exit Date: {this.state.info.DataUltimaSaida}</p>
                                <p>Family: {this.state.info.Familia}</p>
                                <p>Garanty: {this.state.info.Garantia}</p>
                                <p>IVA: {this.state.info.IVA}</p>
                                <p>Location Sugestion: {this.state.info.LocalizacaoSugestao}</p>
                                <p>Brand: {this.state.info.Marca}</p>
                                <p>Model: {this.state.info.Modelo}</p>
                                <p>Stock Movement: {this.state.info.MovStock}</p>
                                <p>Delivery Deadline: {this.state.info.PrazoEntrega}</p>
                                <p>Receiving Quantity: {this.state.info.QtReceber}</p>
                                <p>Reserved Quantity: {this.state.info.QtReservada}</p>
                                <p>Economic Quantity: {this.state.info.QuantEconomica}</p>
                                <p>Maximum Stock: {this.state.info.STKMaximo}</p>
                                <p>Minimum Stock: {this.state.info.STKMinimo}</p>
                                <p>Replacement Stock: {this.state.info.STKReposicao}</p>
                                <p>Current Stock: {this.state.StkActual}</p>
                                <p>Medium Cost Price: {this.state.info.PCMedio}</p>
                                <p>PVP1: {this.state.PVP1}</p>
                                <p>Last Buy Document: {this.state.info.UltimoDocumentoCompra}</p>
                                <p>Last Provider: {this.state.info.UltimoFornecedor}</p>
                            </Segment>

                            <Segment style={{ height: 150 }} >
                                <h4 style={{ height: 50 }}>Total Credit Amount</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.totalCreditAmount)}</p>
                            </Segment>

                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Segment height={10}>
                                <h2>Sales</h2>
                                <ReactTable
                                    data={invoices}
                                    columns={[
                                        {
                                            Header: "Sale ID",
                                            accessor: "id",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/sales/${row.id}`,
                                                    state: { saleInfo: this.getSaleInfo(row.id), sales: this.state.sales }
                                                }}>
                                                    {row.id}
                                                </Link>)
                                        },
                                        {
                                            Header: "Type",
                                            accessor: "type"
                                        },
                                        {
                                            Header: "Date",
                                            accessor: "date",
                                        },
                                        {
                                            Header: "Quantity",
                                            accessor: "quantity"
                                        },
                                        {
                                            Header: "Unit Price €",
                                            accessor: "unitPrice"
                                        },
                                        {
                                            Header: "Credit Amount €",
                                            accessor: "creditAmount"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                            <Segment height={5}>
                                <h2>Purchases</h2>
                                <ReactTable
                                    data={this.state.productSuppliers}
                                    columns={[
                                        {
                                            Header: "Supplier ID",
                                            accessor: "Fornecedor",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/supplier/${row.Fornecedor}`,
                                                    state: { supplierInfo: this.getSupplierInfo(row.Fornecedor), sales: this.state.sales }
                                                }}>
                                                    {row.Fornecedor}
                                                </Link>)
                                        },
                                        {
                                            Header: "Supplier Name",
                                            accessor: "Nome"
                                        },
                                        {
                                            Header: "Entry Date",
                                            accessor: "DataEntrada",
                                        },
                                        {
                                            Header: "Delivery Date",
                                            accessor: "DataEntrega"
                                        },
                                        {
                                            Header: "Unit Price €",
                                            accessor: "PrecUnit"
                                        },
                                        {
                                            Header: "Quantity",
                                            accessor: "Quantidade"
                                        },
                                        {
                                            Header: "Liquid Price €",
                                            accessor: "PrecoLiquido"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );

    }

}


export default ProductsPage;