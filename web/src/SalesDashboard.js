import React, { Component } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import { Bar, Line } from 'react-chartjs-2';
import Select from 'react-select';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './MainDashboard.css';
import ReactTable from "react-table";
import "react-table/react-table.css";

let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

class SalesDashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sales: [],
            allSales: [],
            salesShow: [],
            salesVolume: [],
            salesValuePerCustomer: [],
            avgUnitsValueTransactionPerMonth: [],
            selectedMonth: { label: months[new Date().getMonth()], value: new Date().getMonth() },
            months: [
                { label: "January", value: 0 },
                { label: "February", value: 1 },
                { label: "March", value: 2 },
                { label: "April", value: 3 },
                { label: "May", value: 4 },
                { label: "June", value: 5 },
                { label: "July", value: 6 },
                { label: "August", value: 7 },
                { label: "September", value: 8 },
                { label: "October", value: 9 },
                { label: "November", value: 10 },
                { label: "December", value: 11 }
            ]
        }

        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.getSales = this.getSales.bind(this);
        this.getSaleInfo = this.getSaleInfo.bind(this);
        this.getSalesVolume = this.getSalesVolume.bind(this);
        this.getSalesValuePerCustomer = this.getSalesValuePerCustomer.bind(this);
        this.getAvgUnitsValueTransactionPerMonth = this.getAvgUnitsValueTransactionPerMonth.bind(this);
    }

    componentDidMount() {
        this.getSales();
        this.getSalesVolume();
        this.getSalesValuePerCustomer();
        this.getAvgUnitsValueTransactionPerMonth();
        this.getAllSales();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.selectedMonth.label !== this.state.selectedMonth.label) {
            this.getSales();
            this.getSalesValuePerCustomer();
        }
    }

    handleMonthChange(event) {
        this.setState({ selectedMonth: event });
    }

    getSales() {

        axios.get('http://localhost:3030/getSales?month=' + this.state.selectedMonth.value)
            .then((res) => {

                let salesShow = res.data.map(sale => ({
                    id: sale.InvoiceNo, type: sale.InvoiceType,
                    client: sale.clientName, netTotal: sale.DocumentTotals.NetTotal.toFixed(2), grossTotal: sale.DocumentTotals.GrossTotal.toFixed(2)
                }));

                this.setState({ sales: res.data, salesShow })
            })
            .catch((err) => console.log(err))

    }

    getAllSales() {
        axios.get('http://localhost:3030/getAllSales')
            .then((res) => this.setState({ allSales: res.data }))
            .catch((err) => console.log(err))
    }

    getSaleInfo(invoice_no) {

        let sale;

        for (let i = 0; i < this.state.sales.length; i++) {

            if (this.state.sales[i].InvoiceNo === invoice_no) {
                sale = this.state.sales[i];
            }

        }

        return sale;

    }

    getSalesVolume() {

        axios.get('http://localhost:3030/getSalesVolume')
            .then((res) => this.setState({ salesVolume: res.data }))
            .catch((err) => console.log(err))

    }

    getSalesValuePerCustomer() {

        axios.get('http://localhost:3030/getSalesValuePerCustomer?month=' + this.state.selectedMonth.value)
            .then((res) => this.setState({ salesValuePerCustomer: res.data }))
            .catch((err) => console.log(err))

    }

    getAvgUnitsValueTransactionPerMonth() {

        axios.get('http://localhost:3030/getAvgUnitsValueTransactionPerMonth')
            .then((res) => this.setState({ avgUnitsValueTransactionPerMonth: res.data }))
            .catch((err) => console.log(err))

    }

    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }

    render() {

        let avgSalesValue = 0;
        for (let i = 0; i < this.state.salesValuePerCustomer.length; i++) {
            avgSalesValue += this.state.salesValuePerCustomer[i].saleValue;
        }
        avgSalesValue = avgSalesValue / this.state.salesValuePerCustomer.length;

        let data_bar = {
            labels: months,
            datasets: [{
                label: 'Sales',
                data: this.state.salesVolume,
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }],
            options: {
                title: {
                    display: true,
                    position: 'top',
                    text: 'Sales Volume'
                }
            }
        }

        let data_line_graph = {
            labels: months,
            datasets: [{
                label: 'Avg. Gross Total (K)',
                data: this.state.avgUnitsValueTransactionPerMonth.map(data => data.value / 1000),
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }, {
                label: 'Avg.Unit',
                data: this.state.avgUnitsValueTransactionPerMonth.map(data => data.units),
                backgroundColor: "rgba(54, 162, 235, 0.2)"
            }],
            options: {
                title: {
                    display: true,
                    position: 'top',
                    text: 'Avg. Price &amp; Units per Transaction'
                }
            }
        }

        return (
            <div>
                <Grid columns={2}>
                    <Grid.Row className="first-row-dropdown">
                        <Grid.Column width={13}>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Select
                                className="align-left dropdown"
                                value={this.state.selectedMonth}
                                onChange={this.handleMonthChange}
                                options={this.state.months}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row stretched>
                        <Grid.Column width={6}>
                            <Segment>
                                <Bar
                                    data={data_bar}
                                    width={6}
                                    height={400}
                                    options={{
                                        maintainAspectRatio: false,
                                        title: {
                                            display: true,
                                            text: 'Sales Volume',
                                            fontStyle: 'bold',
                                            fontSize: 15
                                        }
                                    }}
                                />
                            </Segment>
                            <Segment>
                                <h4>Sales Volume</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.salesVolume[this.state.selectedMonth.value])}</p>
                            </Segment>
                            <Segment>
                                <h4>Avg. Sales Value Per Customer</h4>
                                <p className="kpi">{this.getPrettyNumber(avgSalesValue)}</p>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Segment height={10}>
                                <ReactTable
                                    data={this.state.salesShow}
                                    columns={[
                                        {
                                            Header: "Sale ID",
                                            accessor: "id",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/sales/${row.id}`,
                                                    state: { saleInfo: this.getSaleInfo(row.id), sales: this.state.allSales }
                                                }}>
                                                    {row.id}
                                                </Link>)
                                        },
                                        {
                                            Header: "Type",
                                            accessor: "type"
                                        },
                                        {
                                            Header: "Client",
                                            accessor: "client",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/customers/${row.client}`,
                                                    state: { sales: this.state.allSales }
                                                }}>
                                                    {row.client}
                                                </Link>)
                                        },
                                        {
                                            Header: "Net Total",
                                            accessor: "netTotal"
                                        },
                                        {
                                            Header: "Gross Total",
                                            accessor: "grossTotal"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                            <Segment>
                                <Line
                                    data={data_line_graph}
                                    width={10}
                                    height={100}
                                    options={{
                                        maintainAspectRatio: false,
                                        title: {
                                            display: true,
                                            text: 'Avg. Price & Units Per Transaction',
                                            fontStyle: 'bold',
                                            fontSize: 15
                                        }
                                    }}
                                />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div >
        );
    }
}


export default SalesDashboard;